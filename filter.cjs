function filter(inputArray,callback){
    let resultArray=[];
    if(inputArray == undefined || inputArray==null || Array.isArray(inputArray)!= true){
        return resultArray;
    }
    
    for(let index=0;index<inputArray.length;index++){
        const checkValue=callback(inputArray[index],index,inputArray);
        if(checkValue==true){
            resultArray.push(inputArray[index]);
        }
    }

    return resultArray;
}

module.exports=filter;