function find(inputArray,callback){
    

    for(let index=0;index<inputArray.length;index++){
        if(callback(inputArray[index])== true){
            return inputArray[index];
        }
        else{
            continue;
        }
        return undefined;
    }
}

module.exports=find;