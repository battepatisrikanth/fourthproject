
function flatten(inputArray,depth){
    let resultArray=[];
    if(inputArray == undefined || inputArray==null || Array.isArray(inputArray)!= true){
        return resultArray;
    }
    if(depth == undefined){
        depth=1;
    }
    
    // for(let mainIndex=0;mainIndex<depth;mainIndex++){
    for(let index=0;index<inputArray.length;index++){
        if(inputArray[index] == undefined){
            index++;
        }
            
        if(Array.isArray(inputArray[index]) && depth>0 && inputArray[index]!=undefined){
            // for(let eleIndex=0;eleIndex<inputArray[index].length;eleIndex++){
            
            resultArray=resultArray.concat(flatten(inputArray[index],depth-1));
                
            // } 
        }                                  //[1,2,[[3]],,,,,[4]];
        else{
                
            resultArray.push(inputArray[index]);
            
        }

        // depth--;
        
        }
    return resultArray;
}

module.exports=flatten;

