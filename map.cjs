function map(inputArray, callback){
    let resultArray=[];
    if(inputArray == undefined || inputArray==null || Array.isArray(inputArray)!= true){
        return resultArray;
    }
    for(let index=0;index<inputArray.length;index++){
        resultArray[index]=callback(inputArray[index],index,inputArray);
    }
    return resultArray;

}

module.exports=map;