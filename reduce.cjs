function reduce(inputArray,callback,startingValue){
    let startIndex=0;
    if(inputArray == undefined || inputArray==null || Array.isArray(inputArray)!= true){
        return undefined;
    }
    if(startingValue== undefined){
        startingValue=inputArray[0];
        startIndex=1

    }

    for(let index=startIndex;index<inputArray.length;index++){
        
        startingValue=callback(startingValue,inputArray[index],index,inputArray);
    }

    return startingValue;
   
}
module.exports=reduce;